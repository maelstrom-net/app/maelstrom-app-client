import React from 'react';

export const ContentWrapper = props => (
    <div
        className="maelstorm-main-content"
        style={{
            width: '100%',
            padding: '30px 40px 140px 40px',
            backgroundColor: 'rgba(0, 0, 0, 0.75)'
        }}
    >
        {props.children}
    </div>
);
