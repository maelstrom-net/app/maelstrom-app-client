import React from 'react';
import { Link } from 'react-router-dom';

const NavMenu = () => (
    <React.Fragment>
        <li>
            <Link to="/">Home</Link>
        </li>
        <li>
            <a href="/forum">Forum</a>
        </li>
        <li>
            <a href="/shop">Shop</a>
        </li>
        <li>
            <Link to="/login">Login</Link>
        </li>
    </React.Fragment>
);

export default NavMenu;
