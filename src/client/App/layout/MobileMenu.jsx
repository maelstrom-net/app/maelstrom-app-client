import React from 'react';
import NavMenu from './NavMenu';

const MobileMenu = () => (
    <div
        id="nk-nav-mobile"
        className="nk-navbar nk-navbar-side nk-navbar-left-side nk-navbar-overlay-content d-lg-none"
    >
        <div className="nano">
            <div className="nano-content">
                <a href="index.html" className="nk-nav-logo">
                    <img src="assets/images/logo.svg" alt="" width="90" />
                </a>
                <div className="nk-navbar-mobile-content">
                    <ul className="nk-nav">
                        <NavMenu />
                    </ul>
                </div>
            </div>
        </div>
    </div>
);

export default MobileMenu;
