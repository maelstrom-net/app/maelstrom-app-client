import React from 'react';
import NavMenu from './NavMenu';

class Header extends React.Component {
    render() {
        return (
            <header className="nk-header nk-header-opaque">
                <nav
                    id="navmls"
                    className="nk-navbar nk-navbar-top nk-navbar-sticky nk-navbar-transparent nk-navbar-autohide"
                >
                    <div className="container">
                        <div className="nk-nav-table">
                            <a href="index.html" className="nk-nav-logo">
                                <img
                                    src="assets/images/logo.svg"
                                    alt=""
                                    width="90"
                                />
                            </a>

                            <ul
                                className="nk-nav nk-nav-right d-none d-lg-block"
                                data-nav-mobile="#nk-nav-mobile"
                            >
                                <NavMenu />
                            </ul>

                            <ul className="nk-nav nk-nav-right nk-nav-icons">
                                <li className="single-icon d-lg-none">
                                    <a
                                        href="#"
                                        className="no-link-effect"
                                        data-nav-toggle="#nk-nav-mobile"
                                    >
                                        <span className="nk-icon-burger">
                                            <span className="nk-t-1" />
                                            <span className="nk-t-2" />
                                            <span className="nk-t-3" />
                                        </span>
                                    </a>
                                </li>

                                <li className="single-icon">
                                    <a
                                        href="/shop"
                                        className="nk-cart-toggle no-link-effect"
                                    >
                                        <span className="nk-icon-toggle">
                                            <span className="nk-icon-toggle-front">
                                                <span className="ion-android-cart" />
                                            </span>
                                            <span className="nk-icon-toggle-back">
                                                <span className="nk-icon-close" />
                                            </span>
                                        </span>
                                    </a>
                                </li>

                                <li className="single-icon">
                                    <a
                                        to="#"
                                        className="nk-sign-toggle no-link-effect"
                                    >
                                        <span className="nk-icon-toggle">
                                            <span className="nk-icon-toggle-front">
                                                <span className="fa fa-sign-in" />
                                            </span>
                                            <span className="nk-icon-toggle-back">
                                                <span className="nk-icon-close" />
                                            </span>
                                        </span>
                                    </a>
                                </li>

                                <li className="single-icon">
                                    <a
                                        href="#"
                                        className="no-link-effect"
                                        data-nav-toggle="#nk-side"
                                    >
                                        <span className="nk-icon-burger">
                                            <span className="nk-t-1" />
                                            <span className="nk-t-2" />
                                            <span className="nk-t-3" />
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </header>
        );
    }
}

export default Header;
