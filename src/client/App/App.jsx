import React from 'react';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
    faHome,
    faRssSquare,
    faExternalLinkAlt
} from '@fortawesome/free-solid-svg-icons';

import Router from './Router';

library.add(faHome, faRssSquare, faExternalLinkAlt);

class App extends React.Component {
    componentDidMount() {
        window.prerenderReady = true;
    }

    render() {
        return <Router />;
    }
}

export default App;
