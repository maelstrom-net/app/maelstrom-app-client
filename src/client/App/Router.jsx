import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from '@this/src/client/Routes/Home';
import Account from '@this/src/client/Routes/Account';
import Register from '@this/src/client/Routes/Register';
import Login from '@this/src/client/Routes/Login';
import Admin from '@this/src/client/Routes/Admin';
import Header from './layout/Header';
import Footer from './layout/Footer';
import MobileMenu from './layout/MobileMenu';
import { ContentWrapper } from './layout/ContentWrapper';

import conf from '@this/conf/conf';

/**
 * This function is used by our sitemap generator
 * DO NOT REMOVE this method, just change routes to your needs
 * if you want to exclude some routes from sitemap just
 * add them directly inside the <Router>
 */
export const routePaths = () => (
    <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/login/" component={Login} />
        <Route path="/register/" component={Register} />
        <Route path="/account/" component={Account} />
    </Switch>
);

export default props => (
    <Router basename={conf.basePath}>
        <React.Fragment>
            <Header />
            <MobileMenu />
            <ContentWrapper>
                {props.children}
                {routePaths()}
            </ContentWrapper>
            <Footer />
            <Switch>
                <Route path="/app/" component={Admin} />
            </Switch>
        </React.Fragment>
    </Router>
);
